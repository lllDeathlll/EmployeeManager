package main

import "fmt"

type Employee struct {
	Name     string // имя
	Age      int    // возраст
	Position string // позиция
	Salary   int    // зарплата
}

var commands = `
1 - Добавить нового сотрудника
2 - Удалить сотрудника
3 - Вывести список сотрудников
4 - Выйти из программы
`

func main() {
	const size = 512
	empls := [size]*Employee{}
	for {
		cmd := 0
		fmt.Print(commands)
		_, err := fmt.Scanf("%d", &cmd)
		if err != nil {
			println(err)
			return
		}

		switch cmd {
		case 1:
			// Добавляем нового сотрудника
			empl := new(Employee)
			fmt.Println("\nИмя:")
			_, err := fmt.Scanf("%s", &empl.Name)
			if err != nil {
				println(err)
				return
			}
			fmt.Println("Возраст:")
			_, err = fmt.Scanf("%d", &empl.Age)
			if err != nil {
				println(err)
				return
			}
			fmt.Println("Позиция:")
			_, err = fmt.Scanf("%s", &empl.Position)
			if err != nil {
				println(err)
				return
			}
			fmt.Println("Зарплата:")
			_, err = fmt.Scanf("%d", &empl.Salary)
			if err != nil {
				println(err)
				return
			}
			for i := 0; i < size; i++ {
				if empls[i] == nil {
					empls[i] = empl
					break
				}
			}
		case 2:
			fmt.Println("Удаляем сотрудника")
			for i := size - 1; i >= 0; i-- {
				if empls[i] != nil {
					empls[i] = nil
					break
				}
			}
		case 3:
			fmt.Println("Вывод сотрудников")
			for i, empl := range empls {
				if empl != nil {
					fmt.Printf("Employee №%v\n", i)
					fmt.Println(empl.Name)
					fmt.Println(empl.Age)
					fmt.Println(empl.Position)
					fmt.Println(empl.Salary)
					fmt.Println()
				}
			}
		case 4:
			break
		}
	}
}
